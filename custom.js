$(document).ready(function(e)
{
	$(".linkNext").on("click",function(){
		var activeImage = $(".imageVisible");
		var nextImage = activeImage.next();
		  
		 
		if(nextImage.length == 0)
			{ 
			activeImage= $("#carouselImages img").first();
			}	
		
		
		activeImage.removeClass("imageVisible").addClass("imageHide").css("z-index",-5);
			
			
		nextImage.addClass("imageVisible").removeClass("imageHide").css("z-index",5);
		$("#carouselImages img").not([activeImage,nextImage]).css("z-index",1);
		e.preventDefault();	
	});
	
	$(".linkPrevious").on("click",function(){
		var activeImage = $(".imageVisible");
		var nextImage = activeImage.prev();
		
		if(nextImage.length == 0)
			{
			activeImage=$("#carouselImage img").last();
			}
		
		activeImage.removeClass("imageVisible").addClass("imageHide").css("z-index",-5);
		nextImage.addClass("imageVisible").removeClass("imageHide").css("z-index",5);
			
		$("#carouselImages img").not([activeImage,nextImage]).css("z-index",1);
		e.preventDefault();	
	});
	
	
});
